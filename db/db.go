package db

import (
	"fmt"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres" //postgres driver.
)

var instance *gorm.DB

func initDB() (*gorm.DB, error) {
	connectionString := fmt.Sprintf(
		"host=%s port=%s user=%s dbname=%s password=%s",
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"),
		os.Getenv("DB_USERNAME"),
		os.Getenv("DB_DATABASE"),
		os.Getenv("DB_PASSWORD"),
	)

	if os.Getenv("APP_MODE") == "development" || os.Getenv("APP_MODE") == "testing" {
		connectionString = fmt.Sprintf("%s sslmode=disable", connectionString)
	}

	return gorm.Open("postgres", connectionString)
}
