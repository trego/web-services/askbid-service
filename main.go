package main

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		fmt.Printf("No .env file is specified...")
	}

	switch command() {
	case "serve":
		fmt.Println("Serve HTTP")
		break
	case "migrate":
		fmt.Println("Migrating database")
		break
	case "seed":
		fmt.Println("Seed database")
		break
	default:
		fmt.Println("Invalid command")
		break
	}

	fmt.Printf("Askbid Service written in Golang")
}

func command() string {
	args := os.Args[1:]
	if len(args) <= 0 {
		fmt.Println("No argument is specified")
	}

	return args[0]
}
